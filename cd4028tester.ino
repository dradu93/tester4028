#define N_OUTPUT_PORTS 4
#define N_INPUT_PORTS 10
#define ERROR_CODE -1
#define PAUSE_MS 500

byte arduinoOutputPorts[N_OUTPUT_PORTS] = { 10, 11, 12, 13 };
byte arduinoInputPorts[N_INPUT_PORTS] = { 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 };

void setup() {
  // initialize serial communication
  Serial.begin(9600);
  byte i;
  // set output pins
  for(i = 0; i < N_OUTPUT_PORTS; ++i){
    pinMode(arduinoOutputPorts[i], OUTPUT);
  }
  // set input pins
  for(i = 0; i < N_INPUT_PORTS; ++i){
    pinMode(arduinoInputPorts[i], INPUT);
  }
}

void sendData(byte d){
  if((d < 0) || (d > 9)){
    char msg[50];
    sprintf(msg, "Data must be between 0 and 9! Received %d", d);
    Serial.println(msg);
    return;
  }
  
  byte *v = (byte*)calloc(N_OUTPUT_PORTS, sizeof(byte));

  byte i = 0;
  while(d > 0){
    v[i++] = (d & 1);
    d >>= 1;
  }
  
  for(i = 0; i < N_OUTPUT_PORTS; ++i){
    if(v[i]){
      digitalWrite(arduinoOutputPorts[i], HIGH);
    }
    else{
      digitalWrite(arduinoOutputPorts[i], LOW);
    }    
  }
  free(v);
}

byte receiveData(){
  byte i;
  for(i = N_INPUT_PORTS - 1; i >= 0; --i){
    if(digitalRead(arduinoInputPorts[i]) == HIGH){
      return i;
    }
  }
  return ERROR_CODE;
}

void loop() {
  delay(PAUSE_MS);
  if(Serial.available()){
    byte s = Serial.read();
    sendData(s - 48);
    delay(PAUSE_MS);
    byte r = receiveData();
    Serial.println(r);
  }
  /*
  byte i;
  for(i = 0; i < 10; ++i){
    sendData(i);
    delay(PAUSE_MS);
    byte r = receiveData();
    if(r == i){
      Serial.print("Corect pentru ");      
    }
    else{
      Serial.println("Gresit pentru ");
    }
    Serial.println(i);
    delay(PAUSE_MS);
  }
  */
}
