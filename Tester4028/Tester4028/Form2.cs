﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tester4028
{
    public partial class Form2 : Form
    {
        SerialPort port;
        Form previousForm;

        public Form2(SerialPort port, Form previousForm)
        {
            InitializeComponent();
            this.port = port;
            this.previousForm = previousForm;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var data = textBox1.Text;

            if(data == null || string.IsNullOrEmpty(data) || (data.Length > 1))
            {
                MessageBox.Show("Input a number between 0 and 9!");
                return;
            }

            try
            {
                var value = Byte.Parse(data);
                if(value < 0 || value > 9)
                {
                    throw new Exception();
                }

                //var sendData = Byte.Parse(data);
                port.Write(data);
                richTextBox1.AppendText("Sent value " + data + " to the controller...\n");
                System.Threading.Thread.Sleep(1500);
                var r = port.ReadLine().TrimEnd('\r');
                var receivedValue = byte.Parse(r);
                if (receivedValue == value)
                {
                    richTextBox1.AppendText("Received back correctly " + receivedValue + "\n");
                }
                else
                {
                    richTextBox1.AppendText("Received back corrupted value " + receivedValue + "\n");
                }
                               
            }
            catch(Exception ex)
            {
                MessageBox.Show("Input a number between 0 and 9!");
                return;
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            previousForm.Show();
        }
    }
}
