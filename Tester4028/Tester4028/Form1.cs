﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tester4028
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }        

        List<int> BaudRatesList = new List<int>()
        {
            300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, 115200
        };

        SerialPort port;

        private void Form1_Load(object sender, EventArgs e)
        {

            var availableComPorts = SerialPort.GetPortNames();

            foreach(var comPort in availableComPorts)
            {
                comboBox1.Items.Add(comPort);
            }

            foreach(var baudRate in BaudRatesList)
            {
                comboBox2.Items.Add(baudRate);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedItem == null || string.IsNullOrEmpty(comboBox1.SelectedItem.ToString()))
            {
                MessageBox.Show("No COM port selected!");
                return;
            }
            if (comboBox2.SelectedItem == null || string.IsNullOrEmpty(comboBox2.SelectedItem.ToString())){
                MessageBox.Show("No baud rate selected!");
                return;
            }         

            try
            {
                var portName = comboBox1.SelectedItem.ToString();
                var baudRate = int.Parse(comboBox2.SelectedItem.ToString());

                port = new SerialPort(portName, baudRate);
                port.Open();

                this.Hide();
                var form2 = new Form2(port, this);
                form2.Show();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
